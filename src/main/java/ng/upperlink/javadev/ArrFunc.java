package ng.upperlink.javadev;

import java.util.Arrays;

// https://bitbucket.org/elvisols/javadev1/src/master/
public class ArrFunc {

	public static void main(String[] args) {
		// Test Cases...
		int[] myArray = { 1, 3, 6, 4, 1, 2 };
//		int[] myArray = { 1, 2, 3 };
//		int[] myArray = { 5, -1, -3 };
//		int[] myArray = { -1, -3 };
//		int[] myArray = { 1, 2, 4, 5, 9 };
		
		int result2 = getSmallestNonOccurrentInt(myArray);
		
		System.out.println("Smallest non occurring integer all is " + result2);
	
	}
	
	private static int getSmallestNonOccurrentInt(int[] myArr) {
		Arrays.sort(myArr);
		int result = 0;
		int max1Element =  myArr[myArr.length-1];
		
		for(Integer i = 1; i <= max1Element+1; i++) {
			if(!Arrays.stream(myArr).anyMatch(i::equals)) {
				result = i;
				break;
			}
		}
		return result < 1 ? 1 : result;
		
		/*
		// below checks for negative and positive occurrence/comparison
		max2Element =  max2Element < 0 && max1Element > 0 ? 0 : max2Element;
		
		for(int i = max2Element + 1; i < max1Element; i++) {
			if(Arrays.asList(myArr).contains(i)) break;

			result = i; break;
		}
		
		// Get the final result for positive numbers
		result = result == 0 ? max1Element + 1 : result;
		
		// return final result or 1 if the its negative
		return result < 1 ? 1 : result;
		 */
	}
	
}
